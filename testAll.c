#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

int
main(void)
{
  extern CU_SuiteInfo filelibSuite;

  CU_SuiteInfo suites[] = {
    filelibSuite,
    CU_SUITE_INFO_NULL
  };

  CU_initialize_registry();

  CU_register_suites(suites);

  CU_basic_run_tests();

  CU_cleanup_registry();
}
