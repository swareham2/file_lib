#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "filelib.h"

/**
 * Checks to see if file mode is valid
 * @param char *mode string indicating the file mode
 * @return true if valid, false if not valid
 */
static bool is_valid_file_mode(const char *mode)
{
  bool return_value = false;

  // file mode should be one or two characters
  if (strlen(mode) > MODE_LENGTH || strlen(mode) < 1)
  {
    goto exit;
  }

  // enumerate all modes allowable
  char *valid_modes[NUMBER_OF_MODES] =
    {"r", "r+", "a+", "w", "a",  "w+"};

  // check against all valid modes and return true if a match is found
  for (int i = 0; i < NUMBER_OF_MODES; i++)
  {
    if (strncmp(mode, valid_modes[i], MODE_LENGTH) == 0)
    {
      return_value = true;
      goto exit;
    }
  }

  // return false otherwise
  exit:
    return return_value;
}

/**
 * Checks to see if file path is valid
 * @param char *path is the path of the file
 * @return true on valid file path, false otherwise
 */
static bool is_valid_file_path(const char *path)
{
  // file exists
  if (access(path, F_OK) == 0)
    return true;

  return false;
}


int opener(FILE **fp, const char *path, const char *mode)
{
  // default return value is library failure
  int return_value = LIBRARY_FAILURE;

  // make sure that a path and mode were specified
  if(!fp || !mode || !is_valid_file_mode(mode) || !is_valid_file_path(path))
  {
      fprintf(stderr, "Invalid Parameters: ");
      return_value = INVALID_PARAMS;
      goto exit;
  }

  *fp = fopen(path, mode);

  // check that fopen succeeded
  if (!*fp)
  {
    perror("fopen failed: ");
    return_value = THIRD_PARTY_FAIL;
    goto exit;
  }

  return_value = SUCCESS;
  exit:
    return return_value;
}


int closer(FILE **fp)
{
    // default return value is library failure
    int return_value = LIBRARY_FAILURE;

    // make sure that a file pointer was passed in
    if(NULL == *fp)
    {
        fprintf(stderr, "Invalid Parameters");
        return_value = INVALID_PARAMS;
        goto exit;
    }

    int rc = fclose(*fp);
    *fp = NULL;

    // fclose returns 0 on success
    if (0 == rc)
    {
        return_value = SUCCESS;
        goto exit;
    }

    perror("fclose failed: ");

    exit:
      return return_value;
}


int reader(FILE *fp, char **buffer)
{
    // default return value is library failure
    int return_value = LIBRARY_FAILURE;

    // make sure that a file pointer was passed in
    if (NULL == fp)
    {
        fprintf(stderr, "Invalid Parameters");
        return_value = INVALID_PARAMS;
        goto exit;
    }

    *buffer = malloc(sizeof(char) * BUFFER_SIZE);

    // check that malloc succeeded
    if (NULL == *buffer)
    {
      perror("Malloc: ");
      return_value = THIRD_PARTY_FAIL;
      goto exit;
    }

    return_value = fread(*buffer, sizeof(char), BUFFER_SIZE, fp);

    exit:
      return return_value;
}


int writer(FILE *fp, const char *buffer, const int length)
{
    // default return value is library failure
    int return_value = LIBRARY_FAILURE;

    // check parameters
    if(NULL == fp || NULL == buffer || 0 >= length)
    {
        fprintf(stderr, "Invalid Parameters");
        return_value = INVALID_PARAMS;
        goto exit;
    }

    return_value = fwrite(buffer, sizeof(char), length, fp);

    // check that the entire message was written
    if (length != return_value)
    {
        return_value = THIRD_PARTY_FAIL;
        goto exit;
    }

    exit:
      return return_value;
}


int get_attributes(const char *path)
{
    int rv = LIBRARY_FAILURE;

    // check parameters
    if(!path || !is_valid_file_path(path))
    {
        fprintf(stderr, "Invalid Parameters");
        rv = INVALID_PARAMS;
        goto exit;
    }

    struct stat info;
    int rc = stat(path, &info);
    if (rc < 0)
    {
        perror("Stat failed: ");
        rv = THIRD_PARTY_FAIL;
        goto exit;
    }

    // bitwise and-ing
    printf( (info.st_mode & S_IRUSR) ? "r" : "-");
    printf( (info.st_mode & S_IWUSR) ? "w" : "-");
    printf( (info.st_mode & S_IXUSR) ? "x" : "-");
    printf( (info.st_mode & S_IRGRP) ? "r" : "-");
    printf( (info.st_mode & S_IWGRP) ? "w" : "-");
    printf( (info.st_mode & S_IXGRP) ? "x" : "-");
    printf( (info.st_mode & S_IROTH) ? "r" : "-");
    printf( (info.st_mode & S_IWOTH) ? "w" : "-");
    printf( (info.st_mode & S_IXOTH) ? "x\n" : "-\n");

    rv = SUCCESS;

    exit:
      return rv;

}


int set_attributes(const char *path, const mode_t mode)
{
  // default return value is library failure
  int rv = LIBRARY_FAILURE;

  // check parameters
  if(!path || !is_valid_file_path(path))
  {
      fprintf(stderr, "Invalid Parameters");
      rv = INVALID_PARAMS;
      goto exit;
  }

  int rc = chmod(path, mode);
  if (rc != 0)
  {
    perror("chmod() failed: ");
    rv = THIRD_PARTY_FAIL;
  }

  rv = SUCCESS;

  exit:
    return rv;
}


int delete(const char *path)
{
  // default return value is library failure
  int rv = LIBRARY_FAILURE;

  // check parameters
  if(!path || !is_valid_file_path(path))
  {
      fprintf(stderr, "Invalid Parameters");
      rv = INVALID_PARAMS;
      goto exit;
  }

  int rc = remove(path);
  if (rc != 0)
  {
    perror("remove() failed: ");
    rv = THIRD_PARTY_FAIL;
    goto exit;
  }

  rv = SUCCESS;

  exit:
    return rv;
}


int get_dates(const char *path)
{
  // default return value is library failure
  int rv = LIBRARY_FAILURE;

  // check parameters
  if(!path || !is_valid_file_path(path))
  {
      fprintf(stderr, "Invalid Parameters");
      rv = INVALID_PARAMS;
      goto exit;
  }

  struct stat info;
  int rc = stat(path, &info);
  if (rc < 0)
  {
      perror("Stat failed: ");
      rv = THIRD_PARTY_FAIL;
      goto exit;
  }

  printf( "Date created: %s", ctime(&info.st_ctime));
  printf( "Date last modified: %s", ctime(&info.st_mtime));

  rv = SUCCESS;

  exit:
    return rv;
}
