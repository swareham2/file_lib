CFLAGS += -Wall -Wextra -Wpedantic
FILE_PATH = /home/user/Documents/BasicDevProjects/file_lib/testfile

.DEFAULT: filelib.a

# avoid deterministic mode
ARFLAGS = Urv

# create filelib static library
filelib.a: filelib.a(filelib.o)
filelib.o: filelib.h

# create filelib static library and run CUnit automated tests
.PHONY: check
check: testAll
		./$^

testAll: testAll.o testFileLib.o filelib.a
testAll: CFLAGS += -I. -g
testAll: LDFLAGS += -L.
testAll: LDLIBS += -lcunit

# clean up the directory
.PHONY: clean
clean:
		$(RM) *.a *.o testAll manual

# set up the environment
.PHONY: setup
setup:
	sudo apt-get install libcunit1 libcunit1-doc libcunit1-dev

# uses a short program to read a long file and cats the file for comparison
test_reader: test_reader.o filelib.a
	cc -o test_reader test_reader.o filelib.a
	cat testfile

# I need some convincing about the directories and the CC
