# FILE_LIB-  


This library provides a simple interface for file operations and is meant to give individuals familiarity with the linux file system.  

CREATE A FILE LIBRARY THAT IS CAPABLE OF SUPPORTING THE FOLLOWING OPERATIONS:
==============================================================================
* Open
* Read
* Close
* Write
* Get attributes (R/W/X)
* Change attributes (R/W/X)
* Delete
* Get date created
* Get date Modified

REQUIREMENTS:
===============================================================================
* This project must use doxygen to comment all code
* All funtions must have unit test written
* Complete user documentation to include usage of the API must be provided
* Make use of latex to produce a pdf of your doxygen documentation
* All functions must return approriate error codes to user
* No memory leaks may be present when running valgrind --leak-check=full
* Must make use of a make file to build the lib
* Make file must use -Wall -Werror at a minimum
* Must produce a test report with screen shots for your library
* All test cases must pass
