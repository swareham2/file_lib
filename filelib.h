/*! \file   filelib.h
    \brief  A library for performing basic file operations.
*/

#ifndef FILELIB_H
#define FILELIB_H

#include <stdio.h>
#include <sys/stat.h>

enum Status{SUCCESS = 0,
            INVALID_PARAMS = -1,
            THIRD_PARTY_FAIL = -2,
            INVALID_PERMISSIONS = -3,
            LIBRARY_FAILURE = -4};

enum Constants{BUFFER_SIZE = 255,
                MODE_LENGTH = 2,
                NUMBER_OF_MODES = 6};

/**
 * Open a file after appropriate checks and return as parameter
 * @param FILE **fp holds the file pointer
 * @param char *path indicates the file to be opened
 * @param char *mode mode to attempt to open the file in
 * @return status code
 */
int opener(FILE **fp, const char *path, const char *mode);

/**
 * Closes a file
 * @param FILE **fp the file pointer to be closed, set to NULL
 * @return status code
 */
int closer(FILE **fp);

/**
 * Reads from a file
 * The file pointer in question must have read permissions
 * @return bytes read
 */
int reader(FILE *fp, char **buffer);

/**
 * Write a message to a file
 * The file pointer in question must have write permissions
 * @param char *path the path of the file being written to
 * char *message the message to be written
 * @return status code
 */
int writer(FILE *fp, const char *buffer, const int length);

/**
 * Get the attributes of the file
 * @param char *path the path of the file to be examined
 * @return status code
 */
int get_attributes(const char *path);

/**
 * Alter the attributes of the file
 * @param char *path the path of the file to be altered
 * @param char *mode the mode
 * @return status code
 */
int set_attributes(const char *path, const mode_t mode);

/**
 * Delete the file referenced by the given file path
 * @param char *path the full path of the file to be deleted
 * @return status code
 */
int delete(const char *path);

/**
 * Get the date created and date modified for the file specified by the path
 * @param char *path the path specifying the file to be examined
 * @return status code
 */
int get_dates(const char *path);

#endif
