#include <stdio.h>
#include <stdlib.h>

#include "filelib.h"

#define FILE_PATH "/home/user/Documents/BasicDevProjects/file_lib/testfile"

int main(void)
{
  puts("Testing filelib function reader");

  FILE *fp = fopen(FILE_PATH, "r");
  if (NULL == fp)
  {
    fprintf(stderr, "Failed to open file");
  }

  char *buffer = malloc(sizeof(char) * BUFFER_SIZE);
  if (NULL == buffer)
  {
    fprintf(stderr, "Failed to allocate memory");
    goto exit;
  }

  int bytes = 0;

  do
  {
    bytes = reader(fp, &buffer);
    printf("Read %d bytes\n", bytes);
    printf("Read: %s\n", buffer);
  } while(0 < bytes);

  fclose(fp);
  free(buffer);
  exit:
    ;
}
