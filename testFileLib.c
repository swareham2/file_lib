#include <CUnit/CUnit.h>
#include <stdlib.h>
#include <unistd.h>

#include "filelib.h"

#define FILE_PATH "/home/user/Documents/BasicDevProjects/file_lib/testfile"
#define NEW_FILE_PATH "/home/user/Documents/BasicDevProjects/file_lib/newfile"


static void testOpener(void)
{
  FILE *test_fp = NULL;

  // opener() returns SUCCESS with good file path
  CU_ASSERT_EQUAL(opener(&test_fp, FILE_PATH, "r"), 0);

  // opener() returns INVALID_PARAMS with invalid file mode
  CU_ASSERT_EQUAL(opener(&test_fp, FILE_PATH, "p"), INVALID_PARAMS);

  // opener() returns INVALID_PARAMS with invalid file path
  CU_ASSERT_EQUAL(opener(&test_fp, "/home/invalidfilepath", "w"), INVALID_PARAMS);
}


static void testCloser(void)
{
  FILE *test_fp;
  opener(&test_fp, FILE_PATH, "r");

  // closer() returns SUCCESS with good file pointer
  CU_ASSERT_EQUAL(closer(&test_fp), 0);

  // closer() returns THIRD_PARTY_FAIL with NULL file pointer
  CU_ASSERT_EQUAL(closer(&test_fp), INVALID_PARAMS);
}


static void testReader(void)
{
  // set up
  FILE *fp;
  opener(&fp, FILE_PATH, "r");
  char *buffer = NULL;

  // reader() returns some number of bytes read with good file path
  CU_ASSERT(reader(fp, &buffer) > 0);
}


static void testWriter(void)
{
  // set up
  FILE *fp;
  opener(&fp, FILE_PATH, "w");

  // writer() returns SUCCESS with good file path
  CU_ASSERT_EQUAL(writer(fp, "hello", 5), 5);

  // writer() returns INVALID_PARAMS with missing file descriptor or missing
  // message
  CU_ASSERT_EQUAL(writer(NULL, "hi", 2), INVALID_PARAMS);
  CU_ASSERT_EQUAL(writer(fp, NULL, 2), INVALID_PARAMS);
  CU_ASSERT_EQUAL(writer(fp, "hey", 0), INVALID_PARAMS);
}


static void testGetAttr(void)
{
  // get_attributes() returns SUCCESS with good file path
  CU_ASSERT_EQUAL(get_attributes(FILE_PATH), 0);

  // get_attributes() returns INVALID_PARAMS with missing parameter
  CU_ASSERT(get_attributes(NULL) == INVALID_PARAMS);

  // get_attributes() returns INVALID_PARAMS with bad path
  CU_ASSERT(get_attributes(NEW_FILE_PATH) == INVALID_PARAMS);
}


static void testSetAttr(void)
{
  FILE *new_file = NULL;
  new_file = fopen(NEW_FILE_PATH, "w");
  fclose(new_file);

  // set_attributes() returns SUCCESS with good file path
  CU_ASSERT_EQUAL(set_attributes(NEW_FILE_PATH, 0777), SUCCESS);

  // set_attributes() returns INVALID_PARAMS with missing parameter
  CU_ASSERT(set_attributes(NULL, 0777) == INVALID_PARAMS);

  delete(NEW_FILE_PATH);

  // set_attributes() returns INVALID_PARAMS with bad file path
  CU_ASSERT(set_attributes(NEW_FILE_PATH, 0777) == INVALID_PARAMS);
}

static void testDelete(void)
{
  // set up ensures file exists
  FILE *fp = fopen(NEW_FILE_PATH, "w");
  fclose(fp);

  // delete() returns SUCCESS with good file path
  CU_ASSERT_EQUAL(delete(NEW_FILE_PATH), SUCCESS);

  // file no longer exists after delete()
  CU_ASSERT(access(NEW_FILE_PATH, F_OK) == -1);

  // delete() returns INVALID_PARAMS with invalid parameters
  CU_ASSERT(delete(NULL) == INVALID_PARAMS);

  // delete() returns INVALID_PARAMS when trying to delete a file that
  // does not exist
  CU_ASSERT(delete(NEW_FILE_PATH) == INVALID_PARAMS);
}


static void testGetDates(void)
{
  // get_dates() returns SUCCESS on good file path
  CU_ASSERT_EQUAL(get_dates(FILE_PATH), 0);

  // get_dates() returns INVALID_PARAMS with invalid parameters
  CU_ASSERT(get_dates(NULL) == INVALID_PARAMS);

  // get_dates() returns INVALID_PARAMS when trying to access a file that
  // does not exist
  CU_ASSERT(get_dates(NEW_FILE_PATH) == INVALID_PARAMS);
}


static CU_TestInfo filelibTests[] = {
  {"Basic Open", testOpener},
  {"Basic Close", testCloser},
  {"Basic Read", testReader},
  {"Write", testWriter},
  {"Get Attributes", testGetAttr},
  {"Set Attributes", testSetAttr},
  {"Delete", testDelete},
  {"Get Dates", testGetDates},
  CU_TEST_INFO_NULL
};

CU_SuiteInfo filelibSuite = {
  "filelib tests",
  NULL,
  NULL,
  NULL,
  NULL,
  filelibTests
};
